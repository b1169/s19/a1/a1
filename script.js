let getCube = 0;
let number = 2;
getCube = number ** 3;

console.log(`The cube of ${number} is ${getCube}.`);

let address = [258, `Washington Ave`, `NW`, `California`, 90001];

let [houseNumber, street, city, state, zipCode] = address;
console.log(`I live in ${houseNumber} ${street} ${city} ${state} ${zipCode}.`);

let crocodile = {
	name: "Lolong",
	animal: "saltwater crocodile",
	weight: 1075,
	length: "20 ft 3 in"
};

let {name, animal, weight, length} = crocodile;
console.log(`${name} was a ${animal}. He weighed at ${weight} with a measurement of ${length}.`);

let arrayNumbers = [1,2,3,4,5,15];

arrayNumbers.forEach(number => console.log(number));

class Dog {
	constructor(name, age, breed){
		this.name= name,
		this.age= age,
		this.breed= breed

	}
	
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Dachshund";

console.log(myDog);